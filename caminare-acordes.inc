\context ChordNames 
	\chords {
		\set chordChanges = ##t
		% intro
		e1:m e1:m

		% caminare...
		e1:m d1 c2 b2:7 e1:m
		e1:m d1 c2 b2:7 e1:m

		% amo al sennor...
		e1:m d1 e1:m
		e2:m d2 c2 a2:m
		b1:7 b1:7

		% caminare...
		e1:m d1 c2 b2:7 e1:m
		e1:m d1 c2 b2:7 e1:m
		e1:m
	}
