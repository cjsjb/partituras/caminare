\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key e \minor

		R1*2  |
		e' 2 g' 4 b'  |
		a' 2 r4 e' 8 fis'  |
%% 5
		g' 4 g' fis' fis'  |
		e' 2. r4  |
		e' 2 g' 4 b'  |
		a' 2 r4 e' 8 fis'  |
		g' 4 g' fis' fis'  |
%% 10
		e' 2. r4  |
		g' 8 g' g' g' e' 4 e' 8 e'  |
		d' 8 d' 4 d' 8 d' 4 e' 8 d'  |
		b 8 b 4. r4 b 8 b  |
		e' 8 e' 4 e' 8 fis' fis' e' fis'  |
%% 15
		g' 4 r8 g' a' a' g' a'  |
		b' 8 b' ~ b' 2. ~  |
		b' 2. r4  |
		e' 2 g' 4 b'  |
		a' 2 r4 e' 8 fis'  |
%% 20
		g' 4 g' fis' fis'  |
		e' 2. r4  |
		e' 2 g' 4 b'  |
		a' 2 r4 e' 8 fis'  |
		g' 4 g' fis' fis'  |
%% 25
		e' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Ca -- mi -- na -- ré
		en pre -- sen -- cia del Se -- ñor.
		Ca -- mi -- na -- ré
		en pre -- sen -- cia del Se -- ñor.

		A -- mo al Se -- ñor por "que es" -- cu -- cha mi voz su -- pli -- can -- te
		Por -- "que in" -- cli -- na "su o" -- í -- do ha -- cia mí el dí -- a que "lo in" -- vo -- co. __

		Ca -- mi -- na -- ré
		en pre -- sen -- cia del Se -- ñor.
		Ca -- mi -- na -- ré
		en pre -- sen -- cia del Se -- ñor.
	}
>>
