\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key e \minor

		R1*2  |
		e' 2 g' 4 e'  |
		d' 2 r4 a 8 b  |
%% 5
		c' 4 c' dis' dis'  |
		b 2. r4  |
		e' 2 g' 4 e'  |
		d' 2 r4 a 8 b  |
		c' 4 c' dis' dis'  |
%% 10
		e' 2. r4  |
		g' 8 g' g' g' e' 4 e' 8 e'  |
		d' 8 d' 4 d' 8 d' 4 e' 8 d'  |
		b 8 b 4. r4 b 8 b  |
		e' 8 e' 4 e' 8 d' d' d' d'  |
%% 15
		c' 4 r8 c' c' c' b c'  |
		dis' 8 dis' ~ dis' 2. ~  |
		dis' 2. r4  |
		e' 2 g' 4 e'  |
		d' 2 r4 a 8 b  |
%% 20
		c' 4 c' dis' dis'  |
		b 2. r4  |
		e' 2 g' 4 e'  |
		d' 2 r4 a 8 b  |
		c' 4 c' dis' dis'  |
%% 25
		e' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Ca -- mi -- na -- ré
		en pre -- sen -- cia del Se -- ñor.
		Ca -- mi -- na -- ré
		en pre -- sen -- cia del Se -- ñor.

		A -- mo al Se -- ñor por "que es" -- cu -- cha mi voz su -- pli -- can -- te
		Por -- "que in" -- cli -- na "su o" -- í -- do ha -- cia mí el dí -- a que "lo in" -- vo -- co. __

		Ca -- mi -- na -- ré
		en pre -- sen -- cia del Se -- ñor.
		Ca -- mi -- na -- ré
		en pre -- sen -- cia del Se -- ñor.
	}
>>
